#!/bin/bash 
echo '!!! Initialisation de la base de données !!!'
mysql -u root --password=${MYSQL_ROOT_PASSWORD} ${MYSQL_DATABASE} < /db_scripts/SupGo_Database_Final.sql
echo '!!! Base de données initialisée avec succès !!!'