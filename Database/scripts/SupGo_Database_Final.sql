-- Adminer 4.7.6 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;

SET NAMES utf8mb4;

DROP DATABASE IF EXISTS `supgo_database`;
CREATE DATABASE `supgo_database` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `supgo_database`;

DROP TABLE IF EXISTS `action`;
CREATE TABLE `action` (
  `idAction` int NOT NULL AUTO_INCREMENT,
  `UUID` varchar(255) NOT NULL,
  `ActionType` varchar(255) NOT NULL,
  PRIMARY KEY (`idAction`),
  UNIQUE KEY `UUID` (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `idCart` int NOT NULL AUTO_INCREMENT,
  `UUID` varchar(255) NOT NULL,
  `UserUUID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`idCart`),
  UNIQUE KEY `UUID` (`UUID`),
  KEY `UserUUID` (`UserUUID`),
  CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`UserUUID`) REFERENCES `user` (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `idCategory` int NOT NULL AUTO_INCREMENT,
  `UUID` varchar(255) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Department` varchar(255) NOT NULL,
  PRIMARY KEY (`idCategory`),
  UNIQUE KEY `UUID` (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `idComment` int NOT NULL AUTO_INCREMENT,
  `UUID` varchar(255) NOT NULL,
  `Datetime` datetime NOT NULL,
  `nbStars` int NOT NULL,
  `Text` text NOT NULL,
  `ProductUUID` varchar(255) NOT NULL,
  `UserUUID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`idComment`),
  UNIQUE KEY `UUID` (`UUID`),
  KEY `ProductUUID` (`ProductUUID`),
  KEY `UserUUID` (`UserUUID`),
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`ProductUUID`) REFERENCES `product` (`UUID`),
  CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`UserUUID`) REFERENCES `user` (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `discount`;
CREATE TABLE `discount` (
  `idDiscount` int NOT NULL AUTO_INCREMENT,
  `UUID` varchar(255) NOT NULL,
  `Percentage` int NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  `ProductUUID` varchar(255) NOT NULL,
  PRIMARY KEY (`idDiscount`),
  UNIQUE KEY `UUID` (`UUID`),
  KEY `ProductUUID` (`ProductUUID`),
  CONSTRAINT `discount_ibfk_1` FOREIGN KEY (`ProductUUID`) REFERENCES `product` (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `history`;
CREATE TABLE `history` (
  `idHistory` int NOT NULL AUTO_INCREMENT,
  `UUID` varchar(255) NOT NULL,
  `Datetime` datetime NOT NULL,
  `ActionUUID` varchar(255) NOT NULL,
  `ProductUUID` varchar(255) NOT NULL,
  `UserUUID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`idHistory`),
  UNIQUE KEY `UUID` (`UUID`),
  KEY `ActionUUID` (`ActionUUID`),
  KEY `ProductUUID` (`ProductUUID`),
  KEY `UserUUID` (`UserUUID`),
  CONSTRAINT `history_ibfk_1` FOREIGN KEY (`ActionUUID`) REFERENCES `action` (`UUID`),
  CONSTRAINT `history_ibfk_2` FOREIGN KEY (`ProductUUID`) REFERENCES `product` (`UUID`),
  CONSTRAINT `history_ibfk_3` FOREIGN KEY (`UserUUID`) REFERENCES `user` (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `idOrder` int NOT NULL AUTO_INCREMENT,
  `UUID` varchar(255) NOT NULL,
  `Quantity` int NOT NULL,
  `CartUUID` varchar(255) NOT NULL,
  `ProductUUID` varchar(255) NOT NULL,
  `DiscountUUID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idOrder`),
  UNIQUE KEY `UUID` (`UUID`),
  KEY `CartUUID` (`CartUUID`),
  KEY `ProductUUID` (`ProductUUID`),
  KEY `DiscountUUID` (`DiscountUUID`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`CartUUID`) REFERENCES `cart` (`UUID`),
  CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`ProductUUID`) REFERENCES `product` (`UUID`),
  CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`DiscountUUID`) REFERENCES `discount` (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `idProduct` int NOT NULL AUTO_INCREMENT,
  `UUID` varchar(255) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Price` float NOT NULL,
  `Description` text NOT NULL,
  `CategoryUUID` varchar(255) NOT NULL,
  PRIMARY KEY (`idProduct`),
  UNIQUE KEY `UUID` (`UUID`),
  KEY `CategoryUUID` (`CategoryUUID`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`CategoryUUID`) REFERENCES `category` (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `stock`;
CREATE TABLE `stock` (
  `idStock` int NOT NULL AUTO_INCREMENT,
  `UUID` varchar(255) NOT NULL,
  `Quantity` int NOT NULL,
  `ProductUUID` varchar(255) NOT NULL,
  `StoreUUID` varchar(255) NOT NULL,
  PRIMARY KEY (`idStock`),
  UNIQUE KEY `UUID` (`UUID`),
  KEY `ProductUUID` (`ProductUUID`),
  KEY `StoreUUID` (`StoreUUID`),
  CONSTRAINT `stock_ibfk_1` FOREIGN KEY (`ProductUUID`) REFERENCES `product` (`UUID`),
  CONSTRAINT `stock_ibfk_2` FOREIGN KEY (`StoreUUID`) REFERENCES `store` (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `store`;
CREATE TABLE `store` (
  `idStore` int NOT NULL AUTO_INCREMENT,
  `UUID` varchar(255) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Latitude` varchar(255) NOT NULL,
  `Longitude` varchar(255) NOT NULL,
  PRIMARY KEY (`idStore`),
  UNIQUE KEY `UUID` (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `idUser` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `UUID` varchar(255) NOT NULL,
  `cardNumber` varchar(255) DEFAULT NULL,
  `cardExpireDate` date DEFAULT NULL,
  `cardOwner` int DEFAULT NULL,
  `cardCCV` int DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `postalCode` varchar(10) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idUser`),
  UNIQUE KEY `UUID` (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- 2020-05-08 01:44:28